//incase of primitive
// A new memory is created if let keyword is used
/*
let a =1,
let b =a,
let c =1,
console.log(a) //a=10
console.log(b)//b=1
console.log(c)//c=1
*/
//in primitive === produce true if the value are same

//incase of non primitive
// a new memory space is created if a variable is not a copy of another variable
// if a variable is a copy of another variable,the variable share memory
/*
let a =[1,2]
let b =a,
let c =[1,2]
a.push(10)
console.log(a) //a=[1,2,10]
console.log(b)//b=[1,2,10]
console.log(c)//c=[1,2]
*/
//in non primitive === produce true if they share same location memory 