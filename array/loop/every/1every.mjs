let ar1=[9,10,12,18,30]

// output will be true if all of the element return true

let isAllGreaterThan18= ar1.every((value,i)=>{
    if (value > 18){
        return true
    }
    else{
        return false
    }
})
console.log(isAllGreaterThan18)

//check whether we have all even number in the list [2,4,9,6]
let s1=[2,4,9,6]
let allEvenNumber=s1.every((value,i)=>{
    if(value %2===0){
        return true
    }
})
console.log(allEvenNumber)

//check whether all students get pass mark from the list [40,30,80] here pass mark is 40

let p1=[40,30,80]

let mark=p1.every((value,i)=>{
    if(value >= 40){
        return true
    }
    else{
        return false
        
    }
})
console.log(mark)

//check whether a person has smoking habit ["smoking","drinking","biting nails"]
let q1=["smoking","drinking","biting nails"]
let habit=q1.some((value,i)=>{
    if(value==="smoking"){
        return true
    }
    else{
        return false
    }
})
console.log(habit)

//"smoking drinking gajadi"
//check wheather we have smoking habits using some method


let h1="smoking drinking gajadi"


let smokingHabits=h1.split(" ").some((value,i)=>{
    if(value ==="smoking"){
        return true
    }
    else{
        return false
    }

})
console.log(smokingHabits)

//my => My

let m1="my"
let M1=m1.split("")
let mM1=M1.map((value,i)=>{
    if(i===0){
        return value.toUpperCase()
    }
    else{
        return value.toLowerCase()
    }
})
let output=mM1.join("")
console.log(output)

//
let firstLetterCapital=(input) =>{
let inputArr = input.split("")

let inputArrArr=inputArr.map((value,i)=>{
    if(i===0){
        return value.toUpperCase()
    }
    else{
        return value.toLowerCase()
    }
})
let _output=inputArrArr.join("")
return _output
}
let _firstLetterCapital=firstLetterCapital("lllllllll")
console.log(_firstLetterCapital)