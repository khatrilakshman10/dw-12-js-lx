let names = ["Nitan", "Ram", "lakshman"]
//             0    ,    1   ,    2
//array is used to store data of different type or same type
//retrieving all elements
console.log(names)
//retrieving specific elements
console.log(names[0])
console.log(names[1])
console.log(names[2])
//change element of array
names[1]="Hari"
console.log(names)
