let ar1=["a","b","c"]
//        0   1   2
//        -3  -2   -1 at method
console.log(ar1[1])
// console.log(ar1[-2])  it's not possile  

console.log(ar1.flat(-2))
console.log(ar1.flat(-1))
console.log(ar1.flat(-3))

// at method is used to find last element of array