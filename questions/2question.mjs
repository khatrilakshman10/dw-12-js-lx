let products = [
    {
      id: 1,
      title: "Product 1",
      category: "electronics",
      price: 5000,
      description: "This is description and Product 1",
      discount: {
        type: "other1",
      },
    },
    {
      id: 2,
      title: "Product 2",
      category: "cloths",
      price: 2000,
      description: "This is description and Product 2",
      discount: {
        type: "other2",
      },
    },
    {
      id: 3,
      title: "Product 3",
      category: "electronics",
      price: 3000,
      description: "This is description and Product 3",
      discount: {
        type: "other",
      },
    },
  ];

//find the array of id ie  output must be [1,2,3]

let output=products.map((value,i)=>{
    return value.id
})
//console.log(output)

let titles=products.map((value,i)=>{
    return value.title
})
//console.log(titles)

let categories=products.map((value,i)=>{
    return value.category
})
//console.log(categories)

let types=products.map((value,i)=>{
    return value.discount.type

})
//console.log(types)

let prices=products.map((value,i)=>{
  return value.price*3
})
//console.log(prices)

//find those array  whose price is >= 3000 => [
let item=products.filter((value,i)=>{
  if(value.price>= 3000)
    return true
  
})
//if filter and map are used simultaneously then always use filter first

.map((value,i)=>{
  return value.title

})
//console.log(item)

let  item2=products.filter((value,i)=>{
  if(value.price!==5000)
  return true
})
.map((value,i)=>{
  return value.title
})
console.log(item2)

let item3=products.filter((value,i)=>{
  if (value.price===3000)
  return value.price

}).map((value,i)=>{
  return value.category
})
console.log(item3)
/*
map is use to modify the element whereas filter is use to filter elements of input




*/